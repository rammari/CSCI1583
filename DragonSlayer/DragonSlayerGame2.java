import java.util.Scanner;

public class DragonSlayerGame2
{
    public static enum Status{CONTINUE, WIN, LOSE, QUIT};
    
    private static Status gameStatus;
    
    public static int usersSelection;
    public static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args){
        System.out.println("The village is being attacked by monstorus creatures!");
        
        
        
        gameStatus = Status.CONTINUE;
        
        createCharacter();
      while(gameStatus == Status.CONTINUE){
            printMainPrompt();
            System.out.print("Select option ");
            usersSelection = input.nextInt();
            executeAdventureChoice(usersSelection);
            printEndMessage();
       
        }
       
        
    }
    
    public static void createCharacter(){
        
        for (int points = 20; points > 0; points--){
             printCreationPrompt();
            System.out.print("Select option ");
            int playersChoice;
            playersChoice = input.nextInt();
            executeStatChoice(playersChoice);
            
        }
    }
    public static void printMainPrompt(){
        System.out.println("printMainPrompt invoked!");
    }
    public static void executeAdventureChoice(int usersChoice){    
         gameStatus = Status.QUIT;
    }
    public static void printEndMessage(){
        System.out.println("GAME OVER");
    }
    public static void printCreationPrompt(){
        System.out.println("printCreationPrompt invoked!");
       
    }
    public static int executeStatChoice(int playersChoice){
        System.out.println("executeStatChoice invoked!");
        return 0;
    }

}

