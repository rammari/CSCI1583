import java.util.Scanner;

public class DragonSlayerGame3b
{
    public static enum Status{CONTINUE, WIN, LOSE, QUIT};
    
    private static Status gameStatus;
    
    public static int usersSelection;
    public static Scanner input = new Scanner(System.in);
    
    public static final int ADVENTURE = 1;
    public static final int DRAGON = 2;
    public static final int QUIT = 3;
    
    public static int herosLevel = 1;
    public static int herosHealth = 0;
    public static int herosAttackPower = 0;
    public static int herosMagicPower = 0;
    
    public static int BUY_HP = 1;
    public static int BUY_AP = 2;
    public static int BUY_MP = 3;
    
    public static void main(String[] args){
        System.out.println("The village is being attacked by monstorus creatures!");
        
        
        
        gameStatus = Status.CONTINUE;
        
        createCharacter();
      while(gameStatus == Status.CONTINUE){
            printMainPrompt();
            System.out.print("Select option ");
            usersSelection = input.nextInt();
            executeAdventureChoice(usersSelection);
       
        }
       printEndMessage();
        
    }
    
    public static void createCharacter(){
        
        for (int points = 20; points > 0; points--){
             printCreationPrompt(points);
            System.out.print("Select option ");
            int playersChoice;
            playersChoice = input.nextInt();
            executeStatChoice(playersChoice);
            
            
        }
    }
    public static void printMainPrompt(){
        System.out.println("1.) Find adventure");
        System.out.println("2.) Confront the Dragon");
        System.out.println("3.) Quit and go home");
    }
    public static void executeAdventureChoice(int usersChoice){    
        if (usersSelection == ADVENTURE){ 
            generateRandomMonster();
            runCombatLoop();
        }
        else if (usersSelection == DRAGON){
            createDragon();
            runCombatLoop();
        }
        else if (usersSelection == QUIT){
            gameStatus = Status.QUIT;
        }
        else{
            System.out.println("invalid option");
        }
    
         
    }
    public static void printEndMessage(){
        System.out.println("The hero's level: " + herosLevel);
        if (gameStatus == Status.LOSE){
            System.out.println("Defeated!");

        }
        else if (gameStatus == Status.WIN){
            System.out.println("You Won!");
        }
        else if (gameStatus == Status.QUIT){
            System.out.println("Quit.");
        }
    }
    public static void printCreationPrompt(int pointsLeft){
        System.out.println("The hero's health:" + herosHealth);
        System.out.println("The hero's attack power:" + herosAttackPower);
        System.out.println("The hero's magic power:" + herosMagicPower);
        System.out.println("1) +10 Health");
        System.out.println("2) +1 Attack");
        System.out.println("3) +3 Magic");
        System.out.println("Stat points left:" + pointsLeft);
       
    }
    public static int executeStatChoice(int choice){
        int pointsRefund = 0;
        if (choice == BUY_HP){
            herosHealth = herosHealth + 10;
        }
        else if (choice == BUY_AP){
            herosAttackPower = herosAttackPower + 1;
        }
        else if (choice == BUY_MP){
            herosMagicPower += 3;
        }
        else{
            System.out.println("not a valid option");
            pointsRefund += 1;
        }
        
        return pointsRefund;
    }
    
    public static void generateRandomMonster(){
        System.out.println("generateRandomMonster invoked!");
        
    }
    
    public static void createDragon(){
        System.out.println("createDragon invoked!");
    }
    public static void runCombatLoop(){
        System.out.println("runCombatLoop invoked!");
    }
}

