import java.util.Scanner;

public class DragonSlayerGame
{
    public static enum Status{CONTINUE, WIN, LOSE, QUIT};
    
    public static Status gameStatus;
    
    public static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args){
        System.out.println("The village is being attacked by monstorus creatures!");
        
        createCharacter(); 
        
        gameStatus = Status.CONTINUE;
        
      // while(gameStatus == Status.CONTINUE){
            printMainPrompt();
            System.out.print("Select option ");
            int usersSelection;
            usersSelection = input.nextInt();
            executeAdventureChoice(usersSelection);
            printEndMessage();
            
            
       // }
        
        
    }
    
    public static void createCharacter(){
        System.out.println("createCharacter invoked!");
    }
    public static void printMainPrompt(){
        System.out.println("printMainPrompt invoked!");
    }
    public static void executeAdventureChoice(int usersChoice){    
    }
    public static void printEndMessage(){
        System.out.println("GAME OVER");
    }

}

