import java.util.Scanner;

import java.util.Random;

public class DragonSlayerGame4b
{
    public static enum Status{CONTINUE, WIN, LOSE, QUIT};
    
    private static Status gameStatus;
    
    public static int usersSelection;
    public static Scanner input = new Scanner(System.in);
    public static Random randomNumbers = new Random();
    
    public static int GOBLIN = 0;
    public static int ORK = 1;
    public static int TROLL = 2;
    
    public static final int ADVENTURE = 1;
    public static final int DRAGON = 2;
    public static final int QUIT = 3;
    
    public static int herosLevel = 1;
    public static int herosHealth = 0;
    public static int herosAttackPower = 0;
    public static int herosMagicPower = 0;
    
    public static int BUY_HP = 1;
    public static int BUY_AP = 2;
    public static int BUY_MP = 3;
    
    public static String monstersName;
    public static int monstersHealth;
    public static int monstersAttackPower;
    public static int monstersExperiencePointValue;
    
    public static boolean isFighting;
    
    public static void main(String[] args){
        System.out.println("The village is being attacked by monstorus creatures!");
        
        
        
        gameStatus = Status.CONTINUE;
        
        createCharacter();
      while(gameStatus == Status.CONTINUE){
            printMainPrompt();
            System.out.print("Select option ");
            usersSelection = input.nextInt();
            executeAdventureChoice(usersSelection);
       
        }
       printEndMessage();
        
    }
    
    public static void createCharacter(){
        
        for (int points = 20; points > 0; points--){
             printCreationPrompt(points);
            System.out.print("Select option ");
            int playersChoice;
            playersChoice = input.nextInt();
            executeStatChoice(playersChoice);
            
            
        }
    }
    public static void printMainPrompt(){
        System.out.println("1.) Find adventure");
        System.out.println("2.) Confront the Dragon");
        System.out.println("3.) Quit and go home");
    }
    public static void executeAdventureChoice(int usersChoice){    
        if (usersSelection == ADVENTURE){ 
            generateRandomMonster();
            runCombatLoop();
        }
        else if (usersSelection == DRAGON){
            createDragon();
            runCombatLoop();
        }
        else if (usersSelection == QUIT){
            gameStatus = Status.QUIT;
        }
        else{
            System.out.println("invalid option");
        }
    
         
    }
    public static void printEndMessage(){
        System.out.println("The hero's level: " + herosLevel);
        if (gameStatus == Status.LOSE){
            System.out.println("Defeated!");

        }
        else if (gameStatus == Status.WIN){
            System.out.println("You Won!");
        }
        else if (gameStatus == Status.QUIT){
            System.out.println("Quit.");
        }
    }
    public static void printCreationPrompt(int pointsLeft){
        System.out.println("The hero's health:" + herosHealth);
        System.out.println("The hero's attack power:" + herosAttackPower);
        System.out.println("The hero's magic power:" + herosMagicPower);
        System.out.println("1) +10 Health");
        System.out.println("2) +1 Attack");
        System.out.println("3) +3 Magic");
        System.out.println("Stat points left:" + pointsLeft);
       
    }
    public static int executeStatChoice(int choice){
        int pointsRefund = 0;
        if (choice == BUY_HP){
            herosHealth = herosHealth + 10;
        }
        else if (choice == BUY_AP){
            herosAttackPower = herosAttackPower + 1;
        }
        else if (choice == BUY_MP){
            herosMagicPower += 3;
        }
        else{
            System.out.println("not a valid option");
            pointsRefund += 1;
        }
        
        return pointsRefund;
    }
    
    public static void generateRandomMonster(){
        int monstersNumber = randomNumbers.nextInt(3);
        if (monstersNumber == GOBLIN){
            createGoblin();
        }
        else if (monstersNumber == ORK){
            createOrk();
        }
        else if (monstersNumber == TROLL){
            createTroll();
        }
        
    }
    
    public static void createGoblin(){
        monstersName = "Goblin";
        monstersAttackPower = 8 + randomNumbers.nextInt(5);
        monstersHealth = 75 + randomNumbers.nextInt(25);
        monstersExperiencePointValue = 1;
        
    }
    
    public static void createDragon(){
       monstersName = "Dragon";
       monstersAttackPower = 50;
       monstersHealth = 1000;
       monstersExperiencePointValue = 20;
        }
        
    public static void createTroll(){
        System.out.println("createTroll invoked!");
    }
    
    public static void createOrk(){
        monstersName = "Ork";
        monstersAttackPower = 12 + randomNumbers.nextInt(5);
        monstersHealth = 100 + randomNumbers.nextInt(25);
        monstersExperiencePointValue = 3;
    }
    public static void runCombatLoop(){
        isFighting = true;
        while (isFighting && isMonsterAlive() && isPlayerAlive() ){
            runCombatRound();
        }
    }
    
    public static boolean isMonsterAlive(){
        System.out.println("isMonsterAlive invoked!");
        return true;
    }
    
    public static boolean isPlayerAlive(){
        System.out.println("isPlayerAlive invoked!");
        return true;
    }
    
    public static void runCombatRound(){
        System.out.println("runCombatRound invoked!");
        isFighting = false;
    }
}

