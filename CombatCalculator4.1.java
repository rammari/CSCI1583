public class CombatCalculator4
{
        public static void main(String[] args )
        {
                /*Monster data variables*/
                //Declare variable for monster's name and initialize it to "goblin"
                String monstersName = "Zombie";
                //Declare variable for monster's health and initialize it 100
                int storedHealth = 100;
                //Declare variable for monster's attack power and initialize it to 15
                int attackPower = 15;
                
                /*Hero data variables*/
                //Declare variable for Hero's health and initialize it to 100
                int herosHealth = 100;
                //Declare variable for Hero's attack power and initialize it to 12
                int herosAttackPower = 12;
                //Declare variable for Hero's magic power and initialize it to 0
                int herosMagicPower = 0;
                
                /*Report Combat Stats*/
                //Print monster's name
                System.out.println("You are fighting a " + monstersName + "!"); 
                //Print the Monster's health
                System.out.println("The monster HP: " + storedHealth);
                //Print the Player's health
                System.out.println("Your HP: " + herosHealth);
                //Print the Player's magic points
                System.out.println("Your MP: " + herosMagicPower);
                
                //Declare variable for user input and initialize with a new scanner object
                /*Combat menu prompt*/
                System.out.println("Combat Options: ");
                //Print option 1: Sword Attack
                System.out.println("    1.) Sword Attack");
                //Print option 2: Cast Spell
                System.out.println("    2.) Cast Spell");
                //Print option 3: Charge Menu
                System.out.println("    3. Charge Mana");
                //Print option 4: Run Away
                System.out.println("    4. Run Away");
                //Prompt player for action
                System.out.println("What action do you want to perform? 1");
                
                //If player chose option 1, (check with equality operator)
                        //print attack text:
                        //"You strick the <monster name> with your sword for <hero attack> damage"
                        System.out.println("You strick the " + monstersName + " with your sword for " + herosAttackPower +  " damage ");
                        
                //Declare variable for user input and initialize with a new scanner object
                /*Combat menu prompt*/
                System.out.println("Combat Options: ");
                //Print option 1: Sword Attack
                System.out.println("    1.) Sword Attack");
                //Print option 2: Cast Spell
                System.out.println("    2.) Cast Spell");
                //Print option 3: Charge Menu
                System.out.println("    3. Charge Mana");
                //Print option 4: Run Away
                System.out.println("    4. Run Away");
                //Prompt player for action
                System.out.println("What action do you want to perform? 2");
                //Else if player chose option 2, (check with equality operator)
                        //print spell message:
                        //"You cast the weaken spell on the monster."
                        System.out.println("You cast the weaken spell on the " + monstersName + ".");
                      
                      //Declare variable for user input and initialize with a new scanner object
                /*Combat menu prompt*/
                System.out.println("Combat Options: ");
                //Print option 1: Sword Attack
                System.out.println("    1.) Sword Attack");
                //Print option 2: Cast Spell
                System.out.println("    2.) Cast Spell");
                //Print option 3: Charge Menu
                System.out.println("    3. Charge Mana");
                //Print option 4: Run Away
                System.out.println("    4. Run Away");
                //Prompt player for action
                System.out.println("What action do you want to perform? ");
                //Else if player chose option 3, (check with equality operator)
                        //print charging message:
                        //"You focus and charge your magic power."
                        System.out.println("You focus and charge your magic power."); 
                        
                   //Declare variable for user input and initialize with a new scanner object
                /*Combat menu prompt*/
                System.out.println("Combat Options: ");
                //Print option 1: Sword Attack
                System.out.println("    1.) Sword Attack");
                //Print option 2: Cast Spell
                System.out.println("    2.) Cast Spell");
                //Print option 3: Charge Menu
                System.out.println("    3. Charge Mana");
                //Print option 4: Run Away
                System.out.println("    4. Run Away");
                //Prompt player for action
                System.out.println("What action do you want to perform? 4");
                //Else if player chose option 4, (check with equality operator)
                        //print retreat message:
                        //"You run away!"
                       System.out.println("You run away!");
                //Declare variable for user input and initialize with a new scanner object
                /*Combat menu prompt*/
                System.out.println("Combat Options: ");
                //Print option 1: Sword Attack
                System.out.println("    1.) Sword Attack");
                //Print option 2: Cast Spell
                System.out.println("    2.) Cast Spell");
                //Print option 3: Charge Menu
                System.out.println("    3. Charge Mana");
                //Print option 4: Run Away
                System.out.println("    4. Run Away");
                //Prompt player for action
                System.out.println("What action do you want to perform? ");
                //Else the player chose incorrectly
                        //print error message:
                        //"I don't understand that command."
                        System.out.println("I don't understand that command.");
                }        
}