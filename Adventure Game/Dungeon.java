import java.util.Scanner;
public class Dungeon{
    
    static final int numberOfRooms = 9;
        
        static final int north = 0;
        static final int east = 1;
        static final int west = 2;
        static final int south = 3;
        
        static int location = 0;
        
        static String[] roomDescriptions = {
            "You entered into the dungeon. Exits are located on the left and right sides of the room.",
            "You are in the dining room. Entrance is the exit.",
            "You are in the kitchen. Exits are through entrance and to the left of entrance.",
            "You are in the living room.",
            "You are taking the stairs.",
            "You are in the first room. Entrance is exit.",
            "You are in the second room.",
            "You are in the third room.",
            "You are on the balcony. Entrance is exit."
            
        };
        
          static int [] []  exits = {
              {4,2,1,-1},
              {-1,0,-1,-1},
              {3,-1,0,-1},
              {-1,-1,-1,2},
              {6,5,7,0},
              {-1,-1,4,-1},
              {8,-1,-1,4},
              {-1,4,-1,-1},
              {-1,-1,-1,6}
              
          };
    public static void main(String[] args){
         
        Scanner input = new Scanner(System.in);
        
        boolean loopControl = true;
        String usersSelection;
         
        while(loopControl){
            System.out.println(roomDescriptions[location]);
            System.out.println("Which direction would you like to go?");
            System.out.println("N: Go North | S: Go South | W: Go West | E: Go East | Q: Quit Game ");
            usersSelection = input.nextLine();
        
            if(usersSelection.equals("N") | usersSelection.equals("n")){
                if(exits [location][0] != -1){
                    location = exits [location][north];
                }
            else{
                System.out.println("Can't go that way.");
            }
            }
            else if(usersSelection.equals("E") | usersSelection.equals("e")){
                if (exits [location][1] != -1){
                    location = exits [location][east];
                    
            }
            else{
                System.out.println("Can't go that way.");
                }    
            }    
            
            else if (usersSelection.equals("W") | usersSelection.equals("w")){
                if(exits [location][2] != -1){
                    location = exits [location][west];
                    
            }
                else{
                    System.out.println("Can't go that way.");
                }
            }
            else if(usersSelection.equals("S") | usersSelection.equals("s")){
                if (exits [location][3] != -1){
                    location = exits [location][south];
            }
            else{
                System.out.println("Can't go that way.");
                }
            }
            
            
            else if(usersSelection.equals("Q") | usersSelection.equals("q")){
                System.out.println("Goodbye.");
                loopControl = false;
            }
            else{
                System.out.println("Invalid entry.");
            
    }
        }
}
}